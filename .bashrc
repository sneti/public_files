################################################## General Options #####################################################
# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

HISTCONTROL=erasedups
shopt -s histappend
shopt -s histreedit
shopt -s checkwinsize
shopt -s globstar

# Autocompletion
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
  if [ -d ~/.bash_completion.d ]; then
    source ~/.bash_completion.d/*.sh
  fi
fi

complete -cf sudo

# Define colours
C_RED="\[\033[0;31m\]"
C_GREEN="\[\033[0;32m\]"
C_LIGHT_GRAY="\[\033[0;37m\]"
C_RESET="\[\033[0m\]"

C_BROWN="\[\033[0;33m\]"
C_BLUE="\[\033[0;34m\]"
C_PURPLE="\[\033[0;35m\]"
C_CYAN="\[\033[0;36m\]"
C_GRAY="\[\033[1;30m\]"
C_WHITE="\[\033[1;37m\]"
C_YELLOW="\[\033[1;33m\]"

C_LIGHT_BLUE="\[\033[1;34m\]"
C_LIGHT_CYAN="\[\033[1;36m\]"
C_LIGHT_PURPLE="\[\033[1;35m\]"
C_LIGHT_RED="\[\033[1;31m\]"
C_LIGHT_GREEN="\[\033[1;32m\]"

################################################## Prompt ##############################################################
C_USERNAME=$C_LIGHT_GREEN
if [ $(id -u) -eq 0 ]; then
    C_USERNAME=$C_LIGHT_RED
fi

if [ -f /etc/os-release ]; then
    DISTRO=$(cat /etc/os-release | grep "^ID=" | awk -F'=' '{ print $2 }' | sed 's@"@@g')
elif [[ "$(uname -o)" == "Cygwin" ]]; then
    DISTRO='cygwin'
else
    DISTRO='Unknown'
fi

case $DISTRO in
    arch)   C_DISTRO=$C_GREEN ;;
    centos) C_DISTRO=$C_CYAN ;;
    debian) C_DISTRO=$C_RED ;;
    ubuntu) C_DISTRO=$C_PURPLE ;;
    cygwin) C_DISTRO=$C_BROWN ;;
    *)      C_DISTRO=$_GRAY
esac

ISA_ARCH=$(uname -m)
if [[ $ISA_ARCH == x86_64 ]]; then
    ISA_ARCH=""
else
    ISA_ARCH="$C_WHITE, $C_BROWN$ISA_ARCH"
fi

PS1="$C_USERNAME\u $C_WHITE@ $C_LIGHT_PURPLE\h $C_WHITE[$C_DISTRO$DISTRO$ISA_ARCH$C_WHITE] $C_YELLOW\w $C_WHITE\$ $C_RESET"

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

################################################## Exports and Aliases #################################################
export PATH=/sbin:/usr/sbin:$HOME/bin:$PATH
export EDITOR=vim

# This causes sudo to retain aliases. https://wiki.archlinux.org/index.php/Sudo#Passing_aliases
alias sudo='sudo '
alias ll='ls -la'
alias sc='systemctl '

# SSH agent aliases
alias sa='ssh-add'
alias sal='ssh-add -l'
alias sad='ssh-add -D'

# Tmux aliases
alias tls='tmux ls'
alias ta='tmux attach -d'
alias ta0='tmux attach -d -t 0'
alias ta1='tmux attach -d -t 1'
alias ta2='tmux attach -d -t 2'
alias ta3='tmux attach -d -t 3'
alias ta4='tmux attach -d -t 4'
alias ta5='tmux attach -d -t 5'
alias ta6='tmux attach -d -t 6'
alias ta7='tmux attach -d -t 7'
alias ta8='tmux attach -d -t 8'
alias ta9='tmux attach -d -t 9'

# iptables helper script
function ipt {
    ! command -v &>/dev/null iptables && echo "iptables not found. Is iptables present?" && return 1
    ! command -v &>/dev/null iptables-restore && echo "iptables-restore not found. Is (iptables|netfilter)-persistent installed?" && return 1
    case "$DISTRO" in
        ubuntu|debian)
            RULE_FILE="/etc/iptables/rules.v4"
            IPTABLES_SERVICE="netfilter-persistent.service"
            if ! ls -la /etc/alternatives/iptables | grep iptables-legacy >/dev/null 2>&1; then
                echo -e "iptables not set to legacy. Run:\n    update-alternatives --set iptables /usr/sbin/iptables-legacy"
                return 1
            fi ;;
        arch)
            RULE_FILE="/etc/iptables/iptables.rules"
            IPTABLES_SERVICE="iptables.service" ;;
        centos)
            RULE_FILE="/etc/sysconfig/iptables"
            IPTABLES_SERVICE="iptables.service" ;;
        *) echo "Unknown distribution: $DISTRO. Only works on ubuntu, debian, arch, and centos." && return 1 ;;
    esac
    [[ ! -f "$RULE_FILE" ]] && echo "Rule file not found: $RULE_FILE" && return 1
    if ! iptables-restore -t "$RULE_FILE"; then
        echo -e "\nError in iptables-restore. Check rules in $RULE_FILE"
        return 1
    fi
    case "$1" in
        -t|--test) echo "$RULE_FILE is OK" ;;
        -r|--run)
            if systemctl is-active "$IPTABLES_SERVICE" | grep active >/dev/null 2>&1; then
                systemctl restart "$IPTABLES_SERVICE"
            else
                echo "Error: systemd unit not active: $IPTABLES_SERVICE" && return 1
            fi ;;
        *) echo -e "ipt: iptables test and restore.\nUsage:\n    ipt [-t (test) | -r (run)]" && return 1
    esac
}

if [ -f ~/.bash_nongit ]; then
    . ~/.bash_nongit
fi
