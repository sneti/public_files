" directories {
    set undodir=~/.vim.tmp,/tmp "put it in ~/.vim.tmp. If failed put it in /tmp
    set backupdir=~/.vim.tmp,/tmp
    set directory=~/.vim.tmp,/tmp
" }

" vim-plug {
"    call plug#begin('~/.vim/plugged')
"    Plug 'https://github.com/kien/ctrlp.vim.git'
"    Plug 'https://github.com/vim-syntastic/syntastic.git'
"    Plug 'https://github.com/ntpeters/vim-better-whitespace.git'
"    call plug#end()
"

"plugin options {
    " CtrlP
    " Syntastic
    " vim-better-whitespace {
        " Enable
        let g:better_whitespace_enabled=1
        " Do not strip on save
        let g:strip_whitespace_on_save=0
        " Disable on these files
        let g:better_whitespace_filetypes_blacklist=['diff', 'gitcommit', 'unite', 'qf', 'help']
    "}
"}

" display options {
    syntax on               "syntax coloring is a first-cut debugging tool
    colorscheme gruvbox     "In .vim/colors/. Try `desert', 'evening','wombar', 'molokai'

    set background=dark     "Prefer it to lightmode.
    set number              "Turn on numbering
    set wrap                "wrap long lines
    set scrolloff=3         "keep three lines visible above and below
    set ruler showcmd       "give line, column, and command in the status line
    set wildmode=longest:full "make filename-completion more terminal-like
    set wildmenu            "a menu for resolving ambiguous tab-completion
    set wildignore=*.pyc,*.sw[pno],.*.bak,.*.tmp         "files we never want to edit
    set guicursor=i:ver25-iCursor
"}

" cursor  {
    :autocmd InsertEnter,InsertLeave * set cul!     " Highlight the current insert line
    let &t_SI = "\e[5 q"                            " Blinking line cursor on insert mode
    let &t_EI = "\e[2 q"                            " Steady block cursor in normal mode
" }

" searching {
    set incsearch           "search as you type
    set hlsearch            "highlight the search
    set ignorecase          "ignore case
    set smartcase           " ...unless the search uses uppercase letters
    set tags=tags;/

    "Use case-sensitive search for the * command though.
    :nnoremap * /\<<C-R>=expand('<cword>')<CR>\>\C<CR>
    :nnoremap # ?\<<C-R>=expand('<cword>')<CR>\>\C<CR>
" }

" statusline {
    " compare the default statusline: %<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P
    set laststatus=2                "always show the status line
    set statusline =                " clear!
    set statusline +=%<             " truncation point
    set statusline +=%2n:           " buffer number
    set statusline +=%f\            " relative path to file
    set statusline +=%#Error#%m     " modified flag [+], highlighted as error
    set statusline +=%r             " readonly flag [RO]
    set statusline +=%##%y          " filetype [ruby], reset color
    set statusline +=%=             " split point for left and right justification
    set statusline +=row:\ %3l      " current line number
    set statusline +=/%-3L\         " number of lines in buffer
    set statusline +=(%3P),\        " percentage through buffer
    set statusline +=col:\ %v\,\    " current virtual column number (visual count)
    set statusline +=char:\ 0x%B\     " Show current character in hex
" }

" clipboard {
    "Prevent vim from trying to connect to X server. On X forwarded systems, load times can be slow
    "Check vim startup time by running 'vim --startuptime /tmp/vimstartdebug.txt' and checking the file
    set clipboard=autoselect,exclude:.*
" }

" movement options {
    "enable mouse in normal, visual, help, prompt modes
    "I skip insert/command modes because it prevents proper middle-click pasting
    "Disable mouse. Don't want to use it.
    "set mouse=nvrh

    " Moving up/down moves visually.
    " This makes files with very long lines much more manageable.
    nnoremap j gj
    nnoremap k gk
    " Moving left/right will wrap around to the previous/next line.
    set whichwrap=b,s,h,l,<,>,~,[,]
    " Backspace will delete whatever is behind your cursor.
    set backspace=indent,eol,start

    "Bind the 'old' up and down. Use these to skip past a very long line.
    noremap gj j
    noremap gk k
" }

" word definitions {
    " These characters are all part of a word. This makes it useful to surround words with
    " quotes or backticks. Be careful since searching for variables, etc. doens't work. Disabling this for now.
    "
    " set iskeyword+=(
    " set iskeyword+=)
    " set iskeyword+=[
    " set iskeyword+=]
    " set iskeyword+=?
"

" general usability {
    "turn off the annoying "ding!"
    set visualbell
    "allow setting extra option directly in files
    "example: "vim: syntax=vim"
    set modeline
    "don't clobber the buffer when pasting in visual mode
    vmap P p
    vnoremap p pgvy
" }

" common typos {
    " Often I hold shift too long when issuing these commands.
    command! Q q
    command! Qall qall
    command! W w
    command! Wall wall
    command! WQ wq
    command! Wq wq
    command! -bang Redraw redraw!
    nmap Q: :q

    "never use Ex mode -- I never *mean* to press it
    nnoremap Q <ESC>
" }

" multiple files {
    set hidden
    nmap <TAB> :bn<CR>
    nmap <S-TAB> :bd<CR>
    set autoread            "auto-reload files, if there's no conflict
    set shortmess+=IA       "no intro message, no swap-file message

    "replacement for CTRL-I, also known as <tab>
    noremap <C-P> <C-I>
" }

"extra filetypes {
    autocmd BufNewFile,BufRead *.js.tmpl set filetype=javascript
    autocmd BufNewFile,BufRead *.css.tmpl set filetype=css
    autocmd BufNewFile,BufRead *.pxi set filetype=pyrex
    autocmd BufNewFile,BufRead *.md set filetype=markdown
    autocmd BufNewFile,BufRead *.txt set filetype=text
    autocmd BufNewFile,BufRead *.text set filetype=text
" }

"indentation options {
    set expandtab                       "use spaces, not tabs
    set softtabstop=4 shiftwidth=4      "4-space indents

    set shiftround                      "always use a multiple of 4 for indents
    set smarttab                        "backspace to remove space-indents
    set autoindent                      "auto-indent for code blocks
    "DONT USE: smartindent              "it does stupid things with comments

    "smart indenting by filetype, better than smartindent
    filetype on
    filetype indent on
    filetype plugin on

    set textwidth=150
"}

"folding options {
    set foldmethod=indent    "Use indent. syntax doesn't work with many languages
    set foldnestmax=2        "Don't keep folding all the tiny blocks
    set foldlevel=99         "Open all fold by default.
"}

" Key remaps {
    """ ---------------------------------------------------------------------------------------------------
    """ ----------------------------------- Function keys -------------------------------------------------
    " F2, F3: Toggle paste
    nnoremap <F2> <ESC>:colorscheme default <bar> :set paste! <bar> :set nu!<CR>
    nnoremap <F3> <ESC>:set background=dark <bar> :colorscheme gruvbox <bar> :set paste! <bar> :set nu!<CR>
    " F4: Toggle fold/unfold all
    :nnoremap <expr> <F4> &foldlevel ? 'zM' :'zR'
    " F5: Spelling: next word
    nnoremap <expr> <F5> ']s'
    " F6: Spelling: Temporarily ignore all instances of this word
    " Standard shortcuts: 'zg' => adds to permanent dic, 'z=' list suggestions
    nnoremap <expr> <F6> 'zG'
    """ ---------------------------------------------------------------------------------------------------
    """ ------------------------------------ Leader based -------------------------------------------------
    " Map leader to space
    nnoremap <SPACE> <Nop>
    let mapleader=" "
    " Toggle fold/unfold at cursor (all levels)
    :nnoremap <expr> <leader>v 'zA'
    " Toggle Syntastic
    nnoremap <leader>s :SyntasticToggleMode<CR>
    " Surround a word within backticks or * or single quote
    nnoremap <leader>b ciw``<Esc>P
    nnoremap <leader>i ciw**<Esc>P
    nnoremap <leader>q ciw''<Esc>P
    " Toggle spelling
    nnoremap <leader>s <ESC>:set spell!<CR>
" }

" cursor highlight {
    " use bold font for cursor highlight
    hi CursorLine term=bold cterm=bold gui=bold
    hi CursorColumn term=bold cterm=bold gui=bold ctermbg=none
" }

" functions {
    function! s:SaveColors(fname, bang)
        exec 'redir' . a:bang . ' >' a:fname
        silent highlight
        redir END
    endfunction
    command! -bang -nargs=1 -complete=file -bar SaveColors call s:SaveColors(<f-args>, "<bang>")
" }

" Cygwin modifications {
    if has('win32')
        set nocompatible
        set backspace=indent,eol,start
        set backup
        set history=50
        set ruler
        set background=dark
        set showcmd
        set incsearch
        syntax on
        set hlsearch
    endif
" }

